import sys
from urllib.parse import urlparse
import requests
from bs4 import BeautifulSoup

r = requests.get('https://cs.usu.edu/')
soup = BeautifulSoup(r.text)

for i in range(len(soup.findAll('a', href=True))):
    print(soup.findAll('a')[i]['href'])
    parse = urlparse(str(soup.findAll('a')[i]['href']))
    print(parse)

