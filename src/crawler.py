import sys
from urllib.parse import urlparse
import requests
from bs4 import BeautifulSoup


def crawl(url, depth, maxdepth, visited=None):
    if visited is None:
        visited = []
    visited.append(url)

    tab = str('    ' * (maxdepth - depth))

    try:
        web = requests.get(url)
        soup = BeautifulSoup(web.text, features="lxml")
        print(tab + url)

        if depth > 0:

            for i in range(len(soup.findAll('a', href=True))):
                site = str(soup.findAll('a', href=True)[i]['href'])

                parse = urlparse(site)

                if site not in visited:
                    visited.append(site)

                    if parse.scheme == 'https' or parse.scheme == 'http':
                        crawl(site, depth - 1, maxdepth, visited)

                    elif site.startswith('/'):
                        crawl(url + site, depth - 1, maxdepth, visited)

                    elif site.startswith('#'):
                        pass

                    elif 'http://' not in site and len(list(site)) > 0:
                        crawl('http://' + site, depth - 1, maxdepth, visited)

                    else:
                        crawl(site, depth - 1, maxdepth, visited)

    except Exception as e:
        print(str(e))


if len(sys.argv) == 3:
    if sys.argv[2].isnumeric():
        crawl(sys.argv[1], int(sys.argv[2]), int(sys.argv[2]))
    else:
        print('USAGE: crawler.py [URL] [CRAWL DEPTH]')
elif len(sys.argv) == 2:
    crawl(sys.argv[1], 3, 3)
else:
    print('USAGE: crawler.py [URL] [CRAWL DEPTH]')
